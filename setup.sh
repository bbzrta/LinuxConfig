#!/usr/bin/env bash
#by Arsham Rezaei
#18/01/2021

# Variables

pkg_manager=""
pkg_option=""
pkg_confirm=""
pkg_list=""


# Creating the needed folder structure for -per user- configurations.
folder_structure () {
	mkdir /home/"$USER"/.fonts /home/"$USER"/.icons /home/"$USER"/.themes
	chmod 777 /home/"$USER"/.fonts /home/"$USER"/.icons /home/"$USER"/.themes
	clear
}

# Setting the shell and icon theme in place
set_themes () {
	sudo "${pkg_manager}" "${pkg_option}" gnome-shell-extensions "${pkg_confirm}"
	unzip ./Files/themes.zip -d /home/"$USER"/.themes/
	unzip ./Files/icons.zip -d /home/"$USER"/.icons/
}

# Setting up the terminal.
set_terminal () {
  	sudo "${pkg_manager}" "${pkg_option}" alacritty zsh "$pkg_confirm"
	mkdir /home/"$USER"/.config/alacritty
	cp ./Files/dotconfig/alacritty.yml /home/"$USER"/.config/alacritty/
	chsh "$USER" -s /bin/zsh
	mkdir /home/"$USER"/.terminal

	sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

	git clone --depth=1 https://github.com/romkatv/powerlevel10k.git "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k"

	git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting"

	cp ./Files/dotconfig/zshrc /home/"$USER"/.zshrc
}

# Installing all the needed packages
install_packages(){
  	while read -r PACKAGE ; do sudo "$pkg_manager" "$pkg_option" "$PACKAGE" "$pkg_confirm"; done < "$1"
}

install_fonts(){
	unzip ./Files/fonts.zip -d ~/.fonts/
}

set_shortcuts(){
	sudo chmod +x ./Files/keybindings/keybindings.pl
	./Files/keybindings/keybindings.pl -i shortcuts.csv
}

backup_shortcuts(){
	sudo chmod +x ./Files/keybindings/keybindings.pl
	./Files/keybindings/keybindings.pl -e shortcuts.csv
}

weaponise_arch(){
	sudo chmod +x ./Files/strap.sh
	bash ./Files/strap.sh
}

ansible(){
	sudo "$pkg_manager" "$pkg_option" ansible "$pkg_confirm"
	sudo ansible-pull -U https://gitlab.com/bbzrta/4n51ble.git
}

##########################################################
clear

echo "1.Pacman(Arch systems)"
echo "2.Apt(Debian, ubuntu, pop os..)"
echo "3.DNF(RHEL, Fedora..)"
echo "4.Custom"
printf "What package manager do you want to use:"
read -r osname
clear
folder_structure

if [[ $osname -eq 1 ]]; then
  pkg_manager="pacman"
  pkg_option="-S"
  pkg_confirm="--noconfirm"
  pkg_list="Files/Software/pacman.txt"
  weaponise_arch
elif [[ $osname -eq 2 ]]; then
  pkg_manager="apt"
  pkg_option="install"
  pkg_confirm="-y"
  pkg_list="Files/Software/apt.txt"
elif [[ $osname -eq 3 ]]; then
  pkg_manager="dnf"
  pkg_option="install"
  pkg_confirm="-y"
  pkg_list="Files/Software/dnf.txt"
elif [[ $osname -eq 4 ]]; then
  printf "Enter your package manager(dnf, pacman, apt etc..): " ;
   read -r pkg_manager
  printf "Enter your package manager option used to install(-S in pacman, install in apt etc..): ";
   read -r pkg_option
  printf "Enter the Required tag for no confirm installation(optional): ";
   read -r pkg_confirm
  printf "Enter the absolute path to your package list: ";
   read -r pkg_list
fi

while true ; do
	clear
	echo "1.Themes"
	echo "2.Terminal"
	echo "3.Software"
	echo "4.Fonts"
	echo "5.Restore Shortcuts"
	echo "6.Back up shortcuts"
	echo "7.Install hacking tools"
	echo "8.Setup Ansible for further automation"
	echo "0.Quit"
	echo "9.Quit and reboot."
	printf "What do you want to start with: "; read -r task

	if [[  $task -eq 1 ]]; then
		set_themes
	elif [[ $task -eq 2 ]]; then
		set_terminal
	elif [[ $task -eq 3 ]]; then
		install_packages "$pkg_list"
	elif [[ $task -eq 4 ]]; then
		install_fonts
	elif [[ $task -eq 5 ]]; then
		set_shortcuts
	elif [[ $task -eq 6 ]]; then
		backup_shortcuts
	elif [[ $task -eq 7 ]]; then
		install_packages "./Files/Software/weapons.txt"
  	elif [[ $task -eq 8 ]]; then
    ansible
	elif [[ $task -eq 0 ]]; then
		break
	elif [[ $task -eq 9 ]]; then
		chmod 755 ~/.fonts ~/.themes ~/.icons ~/.config
		sudo shutdown -r now
	fi

done
